package com.salsatechnology.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class ProductOrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Retorno do método deve ser de código 400 quando receber informacoes invalidas")
    void createOrder() throws Exception {
        var response = mvc.perform(post("/orders/create")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    @DisplayName("Retorno do método deve ser de código 400 quando receber informacoes invalidas")
    void createOrderList() throws Exception {
        var response = mvc.perform(post("/orders/create-list")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

}
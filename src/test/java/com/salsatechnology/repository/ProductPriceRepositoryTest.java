package com.salsatechnology.repository;

import com.salsatechnology.dto.CreateProductPriceDTO;
import com.salsatechnology.dto.FactoryModel;
import com.salsatechnology.model.ProductPrice;
import com.salsatechnology.model.ProductType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class ProductPriceRepositoryTest {

    @Autowired
    private ProductPriceRepository productPriceRepository;

    @Autowired
    private TestEntityManager em;


    @Test
    void findByProductType() {
        var price =productPriceDataTest(ProductType.SAND_BOARD,25.00,9.0);
        registerPrice(price);

        var listPrice = productPriceRepository.findByProductType(ProductType.SAND_BOARD);
        assertThat(listPrice.size()).isNotNull();
        assertThat(listPrice.stream().findFirst().get().getProductValue()).isPositive();
        assertThat(listPrice.stream().findFirst().get().getProductValue()).isEqualTo(25.00);
    }


    private ProductPrice productPriceDataTest(ProductType productType, Double productValue, Double percCommission){
        CreateProductPriceDTO dto = new CreateProductPriceDTO();
        dto.setProductType(productType);
        dto.setProductValue(productValue);
        dto.setPercCommission(percCommission);

        ProductPrice model = FactoryModel.createModel(dto);
        return model;
    }

    private void registerPrice(ProductPrice price){
        em.persist(price);
    }

}
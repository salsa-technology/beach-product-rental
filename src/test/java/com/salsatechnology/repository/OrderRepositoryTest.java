package com.salsatechnology.repository;

import com.salsatechnology.dto.*;
import com.salsatechnology.model.*;
import com.salsatechnology.service.ProductOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TestEntityManager em;

    @Test
    void searchByFilter() {
        var product = productOrderDataTest("Victor Lima",ProductType.SAND_BOARD,5);
        var price =productPriceDataTest(ProductType.SAND_BOARD,25.00,9.0);
        var order = orderDataTest(product,price,null);

        registerPrice(price);
        registerProductOrder(product);
        registerOrder(order);

        FilterProductOrderDTO filterDTO = new FilterProductOrderDTO();
        filterDTO.setUserName("Victor Lima");

        var listData = orderRepository.searchByFilter(filterDTO);
        assertThat(listData.size()).isGreaterThan(0);
    }

    @Test
    void getListOrder() {
        ListOrder listOrder = listOrderDataTest(new ArrayList<>());
        var product = productOrderDataTest("Victor Lima",ProductType.SAND_BOARD,5);
        var product2 = productOrderDataTest("Victor Lima",ProductType.SAND_BOARD,10);

        var price =productPriceDataTest(ProductType.SAND_BOARD,25.00,9.0);

        var order = orderDataTest(product,price,listOrder);
        var order2 = orderDataTest(product2,price,listOrder);
        listOrder.getListOrders().add(order);
        listOrder.getListOrders().add(order2);

        ListOrder lstOrderSaved = registerListOrder(listOrder);
        registerPrice(price);
        registerProductOrder(product);
        registerProductOrder(product2);
        registerOrder(order);
        registerOrder(order2);

        var listData = orderRepository.getListOrder(lstOrderSaved.getId());
        listData.stream().forEach(e -> System.out.println(e.toString()));
        assertThat(listData.size()).isGreaterThan(1);
    }

    private void registerPrice(ProductPrice price){
        em.persist(price);
    }
    private ProductOrder registerProductOrder(ProductOrder productOrder){
        em.persist(productOrder);
        return productOrder;
    }
    private Order registerOrder(Order order){
        em.persist(order);
        return order;
    }

    private ListOrder registerListOrder(ListOrder listOrder){
        em.persist(listOrder);
        return listOrder;
    }

    private ProductOrder productOrderDataTest(String userName, ProductType productType, Integer timeHour){
        ProductOrderDTO dto = new ProductOrderDTO();
        dto.setUserName(userName);
        dto.setProductType(productType);
        dto.setTimeHour(timeHour);

        ProductOrder model = FactoryModel.createModel(dto);
        return model;
    }

    private ProductPrice productPriceDataTest(ProductType productType,Double productValue,Double percCommission){
        CreateProductPriceDTO dto = new CreateProductPriceDTO();
        dto.setProductType(productType);
        dto.setProductValue(productValue);
        dto.setPercCommission(percCommission);

        ProductPrice model = FactoryModel.createModel(dto);
        return model;
    }

    private Order orderDataTest(ProductOrder productOrder, ProductPrice price, ListOrder listOrder){
        OrderDTO dto = new OrderDTO();
        dto.setStartTime(LocalDateTime.now());
        dto.setProductOrder(productOrder);
        dto.setProductPrice(price);

        Order model = FactoryModel.createModel(dto);
        model.setListOrder(listOrder);
        return model;
    }
    private ListOrder listOrderDataTest(List<Order> listOrder){
        ListOrder list = new ListOrder();
        list.setListOrders(listOrder);
        list.setListTotal(0.0);
        list.setCommissionTotal(0.0);
        list.setUserAmountTotal(0.0);

        return list;
    }
}
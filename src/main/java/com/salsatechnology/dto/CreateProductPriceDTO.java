package com.salsatechnology.dto;

import com.salsatechnology.model.ProductType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Data
public class CreateProductPriceDTO {

    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @Positive
    private Double productValue;

    @NotNull
    private Double percCommission;
}

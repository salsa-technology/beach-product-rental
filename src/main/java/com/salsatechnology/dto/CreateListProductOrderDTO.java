package com.salsatechnology.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class CreateListProductOrderDTO {

    @NotNull
    private List<ProductOrderDTO> listProductOrder;
}

package com.salsatechnology.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.salsatechnology.model.ProductType;

import lombok.Data;

@Data
public class ProductOrderDTO {
	
	@NotBlank
	private String userName;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private ProductType productType;
	
	@NotNull
	private Integer timeHour;
}

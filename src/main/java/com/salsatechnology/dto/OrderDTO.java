package com.salsatechnology.dto;

import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductPrice;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class OrderDTO {

    //id do pedido
    private Long id;

    //Hora que aluguel começou a contar
    private LocalDateTime startTime;

    //Hora que o aluguel finalizou
    private LocalDateTime finalTime;

    private ProductOrder productOrder;

    private ProductPrice productPrice;

    private Double commissionValue;

    private Double percCommission;

    private Double productValue;

    private Double productTotal;

    private String mensagem;


}

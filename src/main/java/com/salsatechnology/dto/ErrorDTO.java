package com.salsatechnology.dto;

import lombok.Data;

@Data
public class ErrorDTO {

    private String field;
    private String message;
    private String classException;

}

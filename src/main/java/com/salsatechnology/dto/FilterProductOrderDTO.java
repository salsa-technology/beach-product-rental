package com.salsatechnology.dto;

import com.salsatechnology.model.ProductType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class FilterProductOrderDTO {

	private Long id;

	private String userName;

	@Enumerated(EnumType.STRING)
	private ProductType productType;

	private Integer timeHour;

}

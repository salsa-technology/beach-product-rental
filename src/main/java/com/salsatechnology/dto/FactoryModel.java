package com.salsatechnology.dto;

import com.salsatechnology.model.ListOrder;
import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductPrice;

import java.util.List;
import java.util.stream.Collectors;

public abstract class FactoryModel {

    public static ProductOrder createModel(ProductOrderDTO productOrderDTO) {
        ProductOrder productOrder = new ProductOrder();
        productOrder.setUserName(productOrderDTO.getUserName());
        productOrder.setProductType(productOrderDTO.getProductType());
        productOrder.setTimeHour(productOrderDTO.getTimeHour());

        productOrder.setProductValue(null);
        productOrder.setProductTotal(null);
        productOrder.setUserAmount(null);
        return productOrder;
    }

    public static ProductPrice createModel(CreateProductPriceDTO createProductPriceDTO) {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setProductValue(createProductPriceDTO.getProductValue());
        productPrice.setProductType(createProductPriceDTO.getProductType());
        productPrice.setPercCommission(createProductPriceDTO.getPercCommission());
        productPrice.setId(createProductPriceDTO.getId());

        return productPrice;
    }


    public static Order createModel(OrderDTO orderDTO) {
        Order order = new Order();
        order.setId(orderDTO.getId());
        order.setProductOrder(orderDTO.getProductOrder());
        order.setProductPrice(orderDTO.getProductPrice());
        order.setStartTime(orderDTO.getStartTime());
        order.setFinalTime(orderDTO.getFinalTime());
        order.setProductValue(orderDTO.getProductValue());
        order.setProductTotal(orderDTO.getProductTotal());
        order.setCommissionValue(orderDTO.getCommissionValue());
        order.setPercCommission(orderDTO.getPercCommission());
        return order;
    }

    public static ListOrder createModel(ListOrderDTO dto) {
        ListOrder listOrder = new ListOrder();
        listOrder.setListOrders(dto.getListOrders().stream().map(e -> (FactoryModel.createModel(e))).collect(Collectors.toList()));
        listOrder.setCommissionTotal(dto.getCommissionTotal());
        listOrder.setListTotal(dto.getListTotal());
        listOrder.setUserAmountTotal(dto.getUserAmountTotal());
        return listOrder;
    }
}

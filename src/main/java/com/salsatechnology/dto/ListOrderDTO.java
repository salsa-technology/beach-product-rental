package com.salsatechnology.dto;

import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductPrice;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class ListOrderDTO {

    //id do pedido
    private Long id;

    //value
    private Double listTotal;
    private Double userAmountTotal;
    private Double commissionTotal;

    private Integer productCount;

    private List<OrderDTO> listOrders;


}

package com.salsatechnology.dto;

import com.salsatechnology.model.ListOrder;
import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductPrice;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public abstract class FactoryDTO {

    public static OrderDTO createDto(Order productOrder){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(productOrder.getId());
        orderDTO.setStartTime(productOrder.getStartTime());
        orderDTO.setFinalTime(productOrder.getFinalTime());
        orderDTO.setProductOrder(productOrder.getProductOrder());
        orderDTO.setProductPrice(productOrder.getProductPrice());
        orderDTO.setCommissionValue(productOrder.getCommissionValue());
        orderDTO.setPercCommission(productOrder.getProductPrice().getPercCommission());
        orderDTO.setProductTotal(productOrder.getProductTotal());
        orderDTO.setProductValue(productOrder.getProductValue());

        orderDTO.setMensagem("Funcionário " +productOrder.getProductOrder().getUserName()+
                " alugou o produto " + productOrder.getProductOrder().getProductType() +" por "+productOrder.getProductOrder().getTimeHour()+"h, " +
                productOrder.getProductOrder().getUserName()+ " vai receber uma comissão no valor de R$"+productOrder.getCommissionValue()
                +" pelo aluguel do produto "+ productOrder.getProductOrder().getProductType());

        return orderDTO;
    }

    public static ListOrderDTO createDto(ListOrder listOrder){
        ListOrderDTO listOrderDTO = new ListOrderDTO();
        listOrderDTO.setId(listOrder.getId());
        listOrderDTO.setListOrders(listOrder.getListOrders().stream().map(e -> FactoryDTO.createDto(e)).collect(Collectors.toList()));
        listOrderDTO.setListTotal(listOrder.getListTotal());
        listOrderDTO.setUserAmountTotal(listOrder.getUserAmountTotal());
        listOrderDTO.setCommissionTotal(listOrder.getCommissionTotal());
        listOrderDTO.setProductCount(listOrder.getProductCount());

        return listOrderDTO;
    }


    public static ProductOrder createDto(ProductOrderDTO productOrderDTO) {
        ProductOrder productOrder = new ProductOrder();
        productOrder.setUserName(productOrderDTO.getUserName());
        productOrder.setProductType(productOrderDTO.getProductType());
        productOrder.setTimeHour(productOrderDTO.getTimeHour());

        productOrder.setProductValue(null);
        productOrder.setProductTotal(null);
        productOrder.setUserAmount(null);
        return productOrder;
    }

    public static ProductOrderDTO createDto(ProductOrder productOrder) {
        ProductOrderDTO productOrderDTO = new ProductOrderDTO();
        productOrderDTO.setUserName(productOrder.getUserName());
        productOrderDTO.setProductType(productOrder.getProductType());
        productOrderDTO.setTimeHour(productOrder.getTimeHour());

//        productOrderDTO.setProductValue(null);
//        productOrderDTO.setProductTotal(null);
//        productOrderDTO.setUserAmount(null);
        return productOrderDTO;
    }

    public static ProductPrice createDto(CreateProductPriceDTO createProductPriceDTO) {
        ProductPrice productPrice = new ProductPrice();
        productPrice.setProductValue(createProductPriceDTO.getProductValue());
        productPrice.setProductType(createProductPriceDTO.getProductType());
        productPrice.setPercCommission(createProductPriceDTO.getPercCommission());
        productPrice.setRegistrationDate(LocalDateTime.now());
        productPrice.setId(createProductPriceDTO.getId());

        return productPrice;
    }

    public static CreateProductPriceDTO createDto(ProductPrice ProductPrice){
        CreateProductPriceDTO createProductPriceDTO = new CreateProductPriceDTO();
        createProductPriceDTO.setId(ProductPrice.getId());
        createProductPriceDTO.setProductValue(ProductPrice.getProductValue());
        createProductPriceDTO.setProductType(ProductPrice.getProductType());
        createProductPriceDTO.setPercCommission(ProductPrice.getPercCommission());

        return createProductPriceDTO;
    }

    public static ErrorDTO createDto(FieldError ex){
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setField(ex.getField());
        errorDTO.setMessage(ex.getDefaultMessage());
        return errorDTO;
    }

    public static ErrorDTO createDto(Exception ex){
        ErrorDTO errorDTO = new ErrorDTO();
        errorDTO.setField(null);
        errorDTO.setMessage(ex.getMessage());
        errorDTO.setClassException(ex.getClass().toString());
        return errorDTO;
    }



}

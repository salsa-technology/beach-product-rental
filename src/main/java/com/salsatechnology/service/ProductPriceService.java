package com.salsatechnology.service;

import com.salsatechnology.dto.FactoryDTO;
import com.salsatechnology.dto.CreateProductPriceDTO;
import com.salsatechnology.model.ProductPrice;
import com.salsatechnology.repository.ProductPriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductPriceService {

    private final ProductPriceRepository productPriceRepository;

    @Transactional
    public ProductPrice create(CreateProductPriceDTO createProductPriceDTO) {
        ProductPrice price = FactoryDTO.createDto(createProductPriceDTO);
        productPriceRepository.save(price);
        return price;
    }

    public CreateProductPriceDTO getPrice(Long id) {
        ProductPrice price = productPriceRepository.findById(id).get();
        return FactoryDTO.createDto(price);
    }

    public void delete(Long id) {
        productPriceRepository.deleteById(id);
    }

    @Transactional
    public ProductPrice edit(CreateProductPriceDTO createProductPriceDTO) {
        ProductPrice planta = productPriceRepository.getReferenceById(createProductPriceDTO.getId());
        planta.updateData(createProductPriceDTO);
        return planta;
    }

    public List<CreateProductPriceDTO> listPrices(Pageable paginacao) {
        return productPriceRepository.findAll(paginacao).map(e -> (FactoryDTO.createDto(e))).toList();
    }
}

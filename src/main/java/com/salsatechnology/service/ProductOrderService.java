package com.salsatechnology.service;

import javax.transaction.Transactional;

import com.salsatechnology.dto.*;
import com.salsatechnology.model.ListOrder;
import com.salsatechnology.model.Order;
import com.salsatechnology.repository.ListOrderRepository;
import com.salsatechnology.repository.OrderRepository;
import com.salsatechnology.repository.ProductPriceRepository;
import com.salsatechnology.specification.SpecificationOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.salsatechnology.repository.ProductOrderRepository;

import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductOrderService {

    private final ProductOrderRepository productOrderRepository;
    private final ProductPriceRepository productPriceRepository;
    private final OrderRepository orderRepository;
    private final ListOrderRepository listOrderRepository;

    @Transactional
    public Order createOrder(ProductOrderDTO productOrderDTO) {
        var listOrderSaved = listOrderRepository.save(new ListOrder());
        var productPrice = productPriceRepository.findByProductType(productOrderDTO.getProductType()).stream().findFirst().get();
        var productOrder = FactoryDTO.createDto(productOrderDTO);

        productOrder.setProductValue(productPrice.getProductValue().longValue());

        Order order = new Order(productOrder,productPrice,listOrderSaved);

        var productOrderSaved = productOrderRepository.save(order.getProductOrder());
        order.setProductOrder(productOrderSaved);
        orderRepository.save(order);

        listOrderSaved.getListOrders().add(order);
        listOrderSaved.calcListOrders();

        return order;
    }

    public List<OrderDTO> listOrders(FilterProductOrderDTO filter) {
        return orderRepository.searchByFilter(filter).stream().map(e -> FactoryDTO.createDto(e)).collect(Collectors.toList());
    }

    public ListOrderDTO getOrder(Long id) {
        ListOrder listOrder = listOrderRepository.getReferenceById(id);
        listOrder.setListOrders(orderRepository.getListOrder(id));
        return FactoryDTO.createDto(listOrder);
    }

    @Transactional
    public ListOrderDTO createOrderList(CreateListProductOrderDTO createListProductOrderDTO) {
        var listOrderSaved = listOrderRepository.save(new ListOrder());
        var listCreatedProductOrder = createListProductOrderDTO.getListProductOrder().stream().map(e -> {
            var productPrice = productPriceRepository.findByProductType(e.getProductType()).stream().findFirst().get();
            var productOrder = FactoryModel.createModel(e);
            var order = new Order(productOrder, productPrice, listOrderSaved);

            productOrder = productOrderRepository.save(order.getProductOrder());
            order.setProductOrder(productOrder);
            orderRepository.save(order);
           return order;
        }).collect(Collectors.toList());

        listOrderSaved.setListOrders(listCreatedProductOrder);
        listOrderSaved.calcListOrders();
        listOrderRepository.save(listOrderSaved);

        ListOrderDTO dto = FactoryDTO.createDto(listOrderSaved);
        dto.setListOrders(listCreatedProductOrder.stream().map(e -> FactoryDTO.createDto(e)).collect(Collectors.toList()));
        return dto;
    }
}

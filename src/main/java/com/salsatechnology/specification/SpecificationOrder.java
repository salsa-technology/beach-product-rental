package com.salsatechnology.specification;

import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductType;
import org.springframework.data.jpa.domain.Specification;

public class SpecificationOrder {

    public static Specification<Order> userName(String userName){
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("productOrder").get("userName"),"%"+userName+"%");
    }

    public static Specification<Order> productType(ProductType type){
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("productOrder").get("productType"), type);
    }

    public static Specification<Order> timeHour(Integer timeHour){
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("productOrder").get("timeHour"), timeHour);
    }

}

package com.salsatechnology.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity(name = "list_orders")
public class ListOrder {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "listOrder")
    private List<Order> listOrders ;

    //value
    private Double listTotal = 0.0;
    private Double userAmountTotal = 0.0;
    private Double commissionTotal = 0.0;
    private Integer productCount = 0;

    public void calcListOrders(){
        ListOrder listCount = new ListOrder();

       this.listOrders.stream().forEach(e -> {

            listCount.setListTotal(e.getProductTotal() + listCount.getListTotal());
            listCount.setCommissionTotal(e.getPercCommission()+ listCount.getCommissionTotal());
        } );

        this.listTotal = listCount.getListTotal();
        this.commissionTotal = listCount.getCommissionTotal();
        this.userAmountTotal = (this.commissionTotal  / this.listTotal) * 100 ;
        this.productCount = this.listOrders.size();
    }

    @Override
    public String toString() {
        return "ListOrder{" +
                "id=" + id +
                ", listOrders=" + listOrders.stream().map(e -> e.toString()) +
                ", listTotal=" + listTotal +
                ", userAmountTotal=" + userAmountTotal +
                ", commissionTotal=" + commissionTotal +
                '}';
    }
}

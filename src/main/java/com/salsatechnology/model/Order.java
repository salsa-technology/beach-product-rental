package com.salsatechnology.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "orders")
@NoArgsConstructor
public class Order {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private ProductOrder productOrder;

    @ManyToOne
    private ProductPrice productPrice;

    @ManyToOne
    private ListOrder listOrder;

    private LocalDateTime startTime;

    private LocalDateTime finalTime;

    private Double productValue;

    private Double productTotal;

    private Double percCommission;

    private Double commissionValue;

    public Order(ProductOrder productOrder, ProductPrice productPrice, ListOrder listOrder) {
        this.setStartTime(LocalDateTime.now());
        this.setProductOrder(productOrder);
        this.setProductPrice(productPrice);
        this.setListOrder(listOrder);
        this.getListOrder().setListOrders(new ArrayList<>());
        this.calcCommission();
    }

    public void calcCommission() {
        var productValue = this.productPrice.getProductValue();
        var timeHour = this.productOrder.getTimeHour();
        var productTotal = productValue * timeHour;
        var commissionValue = productTotal * (this.productPrice.getPercCommission()/100);

        this.productOrder.setProductTotal((long) (productTotal * 100));
        this.productOrder.setProductValue((long) (productValue * 100));
        this.productOrder.setUserAmount(Math.round(commissionValue * 100));

        this.finalTime = startTime.plusHours(timeHour);
        this.commissionValue = commissionValue;
        this.productValue = productValue;
        this.productTotal = productTotal;
        this.percCommission = this.productPrice.getPercCommission();
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productOrder=" + productOrder +
                ", productPrice=" + productPrice +
                ", listOrder=" + listOrder +
                ", startTime=" + startTime +
                ", finalTime=" + finalTime +
                ", productValue=" + productValue +
                ", productTotal=" + productTotal +
                ", percCommission=" + percCommission +
                ", commissionValue=" + commissionValue +
                '}';
    }
}

package com.salsatechnology.model;

import com.salsatechnology.dto.CreateProductPriceDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "product_price")
public class ProductPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ProductType productType;

    private Double productValue;

    private Double percCommission;

    private LocalDateTime registrationDate;


    public void updateData(CreateProductPriceDTO createProductPriceDTO) {
        this.productType = createProductPriceDTO.getProductType();
        this.productValue = createProductPriceDTO.getProductValue();
        this.percCommission = createProductPriceDTO.getPercCommission();
        this.registrationDate = LocalDateTime.now();
    }
}

package com.salsatechnology.exception;

import com.salsatechnology.dto.FactoryDTO;
import com.salsatechnology.dto.FactoryModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ErrorTreatment {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity erro500(NoSuchElementException e){
        return ResponseEntity.internalServerError().body(FactoryDTO.createDto(e));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity erro404(EntityNotFoundException e){
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity erro400(MethodArgumentNotValidException ex){
        var listFieldErrors = ex.getFieldErrors();
        return ResponseEntity.badRequest().body(listFieldErrors.stream().map(e -> (FactoryDTO.createDto(e))));
    }

}

package com.salsatechnology.repository;

import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductPrice;
import com.salsatechnology.model.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {

    List<ProductPrice> findByProductType(ProductType type);

}

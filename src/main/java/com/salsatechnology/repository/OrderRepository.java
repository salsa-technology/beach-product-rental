package com.salsatechnology.repository;

import com.salsatechnology.dto.FilterProductOrderDTO;
import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductType;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {

  //JPQL
   @Query("SELECT o FROM Order o join o.productOrder po where ((po.userName like %:#{#filter.userName}% or :#{#filter.userName} is null) and (po.productType = :#{#filter.productType} or :#{#filter.productType} is null) and (po.timeHour = :#{#filter.timeHour} or :#{#filter.timeHour} is null))")
   List<Order> searchByFilter(@Param("filter") FilterProductOrderDTO filter);


 //JPQL
 @Query("SELECT o FROM Order o  where o.listOrder.id in (:list_order_id)")
 List<Order> getListOrder(Long list_order_id);

}

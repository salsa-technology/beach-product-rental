package com.salsatechnology.repository;

import com.salsatechnology.model.ListOrder;
import com.salsatechnology.model.Order;
import com.salsatechnology.model.ProductOrder;
import com.salsatechnology.model.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListOrderRepository extends JpaRepository<ListOrder, Long> {

}

package com.salsatechnology.controller;

import com.salsatechnology.dto.CreateProductPriceDTO;
import com.salsatechnology.model.ProductPrice;
import com.salsatechnology.service.ProductPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/price")
public class ProductPriceController {

    @Autowired
    ProductPriceService productPriceService;

    @GetMapping
    public ResponseEntity<List<CreateProductPriceDTO>> listPrices(@PageableDefault(size = 10,sort = {"productValue"}) Pageable page){
        List<CreateProductPriceDTO> list = productPriceService.listPrices(page);
        return ResponseEntity.ok(list);
    }

    @PutMapping
    @Transactional
    public ResponseEntity<CreateProductPriceDTO> editPrice(@RequestBody @Valid CreateProductPriceDTO createProductPriceDTO){
        ProductPrice price = productPriceService.edit(createProductPriceDTO);
        return ResponseEntity.ok(createProductPriceDTO);
    }

    @PostMapping
    @Transactional
    public ResponseEntity createPrice(@RequestBody CreateProductPriceDTO createProductPriceDTO, UriComponentsBuilder uriBuilder){
        ProductPrice price = productPriceService.create(createProductPriceDTO);
        URI uri = uriBuilder.path("/price/{id}").buildAndExpand(price.getId()).toUri();
        return  ResponseEntity.created(uri).body(price);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CreateProductPriceDTO> getPrice(@PathVariable Long id){
        CreateProductPriceDTO price = productPriceService.getPrice(id);
        return ResponseEntity.ok(price);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePrice(@PathVariable Long id){
        productPriceService.delete(id);
        return ResponseEntity.noContent().build();
    }



}

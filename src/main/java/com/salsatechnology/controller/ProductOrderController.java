package com.salsatechnology.controller;

import javax.validation.Valid;

import com.salsatechnology.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.salsatechnology.service.ProductOrderService;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class ProductOrderController {
	
	@Autowired
	private ProductOrderService productOrderService;

	@PostMapping("/create")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<OrderDTO> createOrder(@RequestBody @Valid ProductOrderDTO productOrderDTO) {
		var order = productOrderService.createOrder(productOrderDTO);
		return ResponseEntity.ok(FactoryDTO.createDto(order));
	}

	@PostMapping("/create-list")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ListOrderDTO> createOrderList(@RequestBody @Valid CreateListProductOrderDTO createListProductOrderDTO) {
		var order = productOrderService.createOrderList(createListProductOrderDTO);
		return ResponseEntity.ok(order);
	}

	@GetMapping()
	public ResponseEntity<List<OrderDTO>> listOrders( @RequestBody FilterProductOrderDTO filter){
		return ResponseEntity.ok(productOrderService.listOrders(filter));
	}

	@GetMapping("/{id}")
	public ResponseEntity<ListOrderDTO> getOrder(@PathVariable Long id){
		ListOrderDTO order = productOrderService.getOrder(id);
		return ResponseEntity.ok(order);
	}

}

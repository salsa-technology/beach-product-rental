create table list_orders
(
    id                bigint not null auto_increment,
    commission_total  double precision,
    list_total        double precision,
    product_count     integer,
    user_amount_total double precision,
    primary key (id)
) engine=InnoDB;

alter table orders
    add constraint FKrr6nrcd1fssqlycis4o81fb6x foreign key (list_order_id) references list_orders (id);
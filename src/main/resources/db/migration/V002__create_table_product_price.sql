create table product_price
(
    id                bigint not null auto_increment,
    perc_commission   double precision,
    product_type      varchar(255),
    product_value     double precision,
    registration_date datetime(6),
    primary key (id)
) engine=InnoDB;

insert into product_price
(perc_commission, product_type, product_value, registration_date)
values (15.6, 'SURFBOARD', 50.0, CURRENT_TIME ());

insert into product_price
(perc_commission, product_type, product_value, registration_date)
values
 (5.0, 'BEACH_CHAIR', 35.0, CURRENT_TIME ());

insert into product_price
(perc_commission, product_type, product_value, registration_date)
values
(10.3, 'SUNSHADE', 40.0, CURRENT_TIME ());

insert into product_price
(perc_commission, product_type, product_value, registration_date)
values
(9, 'SAND_BOARD', 25, CURRENT_TIME ());

insert into product_price
(perc_commission, product_type, product_value, registration_date)
values
(8.1, 'BEACH_TABLE', 25.0, CURRENT_TIME ());
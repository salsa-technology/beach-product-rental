create table orders
(
    id               bigint not null auto_increment,
    commission_value double precision,
    final_time       datetime(6),
    perc_commission  double precision,
    product_total    double precision,
    product_value    double precision,
    start_time       datetime(6),
    list_order_id    bigint,
    product_order_id bigint,
    product_price_id bigint,
    primary key (id)
) engine=InnoDB

alter table orders
    add constraint FKt9pocabfq29dvm6ybbygxly7q foreign key (product_order_id) references product_order (id);

alter table orders
    add constraint FKm18uedfxtuc0i8ldylww88hwa foreign key (product_price_id) references product_price (id);


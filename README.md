# beach-product-rental

Dev: Victor Lima

Gitlab: https://gitlab.com/salsa-technology/beach-product-rental

Api Doc doc: http://localhost:8080/swagger-ui/index.html

## Esse é um projecto Spring Boot desenvolvido como teste:
Foi Utilizado nesse projeto o pattern Factory Abstract para criação dos DTOS sendo responsabilidade das classes FactoryDTO e FactoryModel.

## Diagrama de classes
![](doc/product-order-diagram.jpg)

## Cenário: A empresa XYZ trabalha com aluguel de produtos que são utilizados na praia.

1. cada produto possui um valor cobrado por hora, o sistema aceita apenas valores inteiros para o campo hora;
2. funcionário recebe uma comisão baseada no valor total cobrado pelo produto.
3. o funcionário pode registrar mais de um produto por requisição e recebe o total da comissão agrupada

## Descrição dos campos da classe ListOrder.
1. listOrders: lista de Orderns de produto da venda;
2. listTotal: valor total dos produtos do pedido;
3. userAmountTotal: percentual de comissão recebido pelo funcionário no pedido;
4. commissionTotal: Valor total da comissão recebido pelo funcionário no pedido;
5. productCount: Quantidade de produtos alocados no pedido; 

## Descrição dos campos da classe Order.
1. startTime: Data e hora que iniciou o aluguel do produto;
2. finalTime: Data e hora que o aluguel será finalizado;
3. productValue: preço do produto na data da venda;
4. productTotal: valor total do produto na venda;
5. percCommission: percentual de comissão aplicado na venda;
6. commissionValue: Comissão a ser recebida pelo usuário(Funcionário);
7. productPrice: Preço registrado na venda;
8. productOrder: Produto registrado na venda;

## Descrição dos campos da classe ProductOrder.
1. userName: Login do usuário;
2. productType: Tipo do produto;
3. timeHour: Tempo em horas que o produto ficou locado;
4. productValue: Valor do produto por hora;
5. productTotal: Valor do produto vezes tempo em horas locado;
6. userAmount: Comissão a ser recebida pelo usuário(Funcionário);

## Descrição dos campos da classe ProductPrice.
1. productType: Tipo do produto;
2. productValue: Preço do produto;
3. percCommission: Percentual de comissão do produto;
4. registrationDate: data que o preço foi registrado;

## Produtos e comissões:
Para SURFBOARD:
Preço por hora: R$50,00
Porcentagem funcionário: 15.6%

Para BEACH_CHAIR
Preço por hora: R$35,00
Porcentagem funcionário: 5%

Para SUNSHADE:
Preço por hora: R$40,00
Porcentagem funcionário: 10.3%

Para SAND_BOARD:
Preço por hora: R$25,00
Porcentagem funcionário: 9%

Para BEACH_TABLE:
Preço por hora: R$25,00
Porcentagem funcionário: 8.1%

## Exemplo:
Funcionário Pedro alugou o produto BEACH_TABLE por 4h e o produto BEACH_CHAIR por 4h
Pedro vai receber uma comissão no valor de R$31,20 pelo aluguel do produto BEACH_TABLE e uma comissão no valor de R$7,00 pelo aluguel do produto BEACH_CHAIR.
